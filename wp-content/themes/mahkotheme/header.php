<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
	   		if (get_the_id() == 112) {
	      		echo 'Home - '; bloginfo('description');
	     	} else {
	     		
	     		if (function_exists('is_tag') && is_tag()) {
		         	single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
			    elseif (is_archive()) {
			        wp_title(''); echo ' Archive - '; }
			    elseif (is_search()) {
			        echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
			    elseif (!(is_404()) && (is_single()) || (is_page())) {
			        wp_title(''); echo ' - '; }
			    elseif (is_404()) {
			        echo 'Not Found - '; }  
			    if (is_home()) {
			        bloginfo('name'); echo ' - '; bloginfo('description'); }
			    else {
			        bloginfo('name'); }
			    if ($paged>1) {
			        echo ' - page '. $paged; }
	     	}
		    
		   ?>
	</title>
	
	<script type="text/javascript">
		//<![CDATA[
		try {
			if (!window.CloudFlare) {
				var CloudFlare = [{
					verbose: 0,
					p: 1424538250,
					byc: 0,
					owlid: "cf",
					bag2: 1,
					mirage2: 0,
					oracle: 0,
					paths: {
						cloudflare: "/cdn-cgi/nexp/dok3v=1613a3a185/"
					},
					atok: "dbcca247d7148b68495c6d6050c80ebf",
					petok: "64e1c9d35edf0b12c13e37427244d53cb772f1de-1424848388-1800",
					zone: "everislabs.com",
					rocket: "0",
					apps: {
						"ga_key": {
							"ua": "UA-52817464-1",
							"ga_bs": "1",
						}
					}
				}];
				!function (a, b) {
					a = document.createElement("script"), b = document.getElementsByTagName("script")[0], a.async = !0, a.src = "<?php echo get_template_directory_uri(); ?>/js/cloudflare.min.js", b.parentNode.insertBefore(a, b)
				}()
			}
		} catch (e) {};
		//]]>
	</script>
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css">

	<link rel='stylesheet' id='contact-form-7-group-css' href='<?php echo get_template_directory_uri(); ?>/css/minify-b1-contact-form-7-53540a9935492488edfbcd56d54033c3.css' type='text/css' media='all'/>
	<style id='rs-plugin-settings-inline-css' type='text/css'>.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}</style>
	<link rel='stylesheet' id='woocommerce-smallscreen-group-css' href='<?php echo get_template_directory_uri(); ?>/css/minify-b1-woocommerce-smallscreen-cb9b39345511a0882314326ca30a08b7.css' type='text/css' media='only screen and (max-width: 768px)'/>
	<link rel='stylesheet' id='redux-google-fonts-css' href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%2C300italic%2C400italic%2C600italic%2C700italic%2C800italic&#038;ver=1405364468' type='text/css' media='all'/>

	<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/style.css' type='text/css' media='all'/>

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>
	<style type="text/css" media="screen">
		@media all {
			html { margin-top: 0 !important; }
		}
	</<style>
		
	</style>

</head>

<body <?php body_class(); ?>>
	<div id="wrap">
			<div class="main-inner-content">
				<header id="header" class="section cover">

					<div class="logo-header">
						<div class="container">
							<div class="logo-container clearfix">
								<?php 
									if ( function_exists( 'ot_get_option' ) ) {
										$logo = ot_get_option( 'logo' );
										$phone = ot_get_option( 'phone' );
										$email = ot_get_option( 'email' );
										$facebook = ot_get_option( 'facebook' );
										$twitter = ot_get_option( 'twitter' );
									}
								?>
								<div class="logo pull-left">
									<p class="site-title" itemprop="headline">
										<a title="Mahko" rel="home" href="/">
											<?php if (empty($logo)) { ?>
												<img class="logo_standard" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Mahko"/>
											<?php } else { ?>
												<img class="logo_standard" src="<?php echo $logo; ?>" alt="Mahko"/>
											<?php } ?>	
										</a>
									</p>
									<span class="site-desc">Mahko</span>
								</div> 
								<div class="social-info pull-right hidden-xs hidden-sm">
									<ul class="social textcolor list-unstyled">
										<li class="phone">
											<a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone"></i><span><?php echo $phone; ?></span></a>
										</li>
										<li class="mail active">
											<a href="mailto:<?php echo $email; ?>">
												<i class="fa fa-envelope"></i><span><?php echo $email; ?></span>
											</a>
										</li>
										<li class="facebook">
											<a href="<?php echo $facebook; ?>">
												<i class="fa fa-facebook"></i><span>follow on facebook</span>
											</a>
										</li>
										<li class="twitter">
											<a href="<?php echo $twitter; ?>">
												<i class="fa fa-twitter"></i><span>follow on twitter</span>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa"><img src="<?php echo get_template_directory_uri(); ?>/images/eng-flag.jpg" alt=""></i><span>English</span>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa"><img src="<?php echo get_template_directory_uri(); ?>/images/por-flag.jpg" alt=""></i><span>Portugese</span>
											</a>
										</li>
									</ul>
								</div>
							</div> 
						</div> 
					</div> <!-- END LOGO-HEADER -->
					<div class="site-menu my-menu" >
						<div class="container">
							<div class="site-menu-inner clearfix">
								<div class="menu-container default pull-left">
									<div class="site-menu-container">
										<nav class="hidden-xs hidden-sm" itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope" role="navigation">
											<?php
												$menu = array(
														'menu' 					=> 'main_nav',
														'container'				=> '',
														'menu_class'			=> 'sf-menu nav nav-tabs list-unstyled clearfix',
														'menu_id'				=> 'menu-main_menu',
													); 
												wp_nav_menu($menu); 
											?>
										</nav>
									</div>
								</div>
							</div> 
						</div> 
					</div> <!-- END SITE-MENU -->
				
				
