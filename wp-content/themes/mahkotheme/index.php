<?php get_header(); ?>
				<div class="container"> 
						<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;max-height:650px;">
							<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:650px;height:650px;">
								<ul>  
									<li data-transition="slotfade-vertical" data-slotamount="7" data-masterspeed="300" data-thumb="" data-saveperformance="off" data-title="Slide">

										<img src="http://themes.everislabs.com/erika/wp-content/plugins/revslider/images/transparent.png" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">


										<div class="tp-caption sfb" data-x="0" data-y="0" data-voffset="7" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2;"><img src="<?php echo get_template_directory_uri(); ?>/images/slider-bg.jpg" alt="">
										</div>

										<div class="tp-caption erika_big_title randomrotate tp-resizeme" data-x="691" data-y="277" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">erika
										</div>

										<div class="tp-caption erika_small_title sft tp-resizeme" data-x="604" data-y="258" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="words" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">Say hello to Multipurpose WordPress Theme
										</div>

										<div class="tp-caption erika_p sfb tp-resizeme" data-x="619" data-y="355" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: pre-wrap;">Proin ut ligula vel nunc egestas porttitor.
											Morbi lectus risus, iaculis vel, suscipit quis, luctus non,massa.
											Fusce ac turpis quis ligula lacinia aliquet.
										</div>

										<div class="tp-caption erika_p sfb tp-resizeme" data-x="687" data-y="464" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6; max-width: auto; max-height: auto; white-space: pre-wrap;"><a class="button" href="#"><span>View all features</span></a> <a class="button black" href="http://go.everislabs.com/erikawp"><span>Purchase</span></a>
										</div>
									</li>

									<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-thumb="" data-saveperformance="off" data-title="Slide">

										<img src="http://themes.everislabs.com/erika/wp-content/plugins/revslider/images/transparent.png" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">


										<div class="tp-caption sfb" data-x="654" data-y="90" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2;"><img src="http://themes.everislabs.com/erika/wp-content/uploads/2014/07/slide_2.png" alt="">
										</div>

										<div class="tp-caption erika_big_title randomrotate tp-resizeme" data-x="95" data-y="306" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Multipurpose
										</div>

										<div class="tp-caption erika_small_title sft tp-resizeme" data-x="428" data-y="290" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">Use Erika for any purpose
										</div>

										<div class="tp-caption erika_p1 sfb tp-resizeme" data-x="190" data-y="386" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: pre-wrap;">Purchase and using this theme for any purpose you want. Support news
											portal, business site and ecommerce.
										</div>
									</li>
								</ul>
								<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> 
							</div>
							<script type="text/javascript">

								/******************************************
									-	PREPARE PLACEHOLDER FOR SLIDER	-
									******************************************/


									var setREVStartSize = function() {
										var	tpopt = new Object();
										tpopt.startwidth = 1170;
										tpopt.startheight = 650;
										tpopt.container = jQuery('#rev_slider_1_1');
										tpopt.fullWidth = "on";
										tpopt.forceFullWidth="on";

										tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
									};

									/* CALL PLACEHOLDER */
									setREVStartSize();


									var tpj=jQuery;
									tpj.noConflict();
									var revapi1;

									tpj(document).ready(function() {

										if(tpj('#rev_slider_1_1').revolution == undefined)
											revslider_showDoubleJqueryError('#rev_slider_1_1');
										else
											revapi1 = tpj('#rev_slider_1_1').show().revolution(
											{
												dottedOverlay:"none",
												delay:9000,
												startwidth:1170,
												startheight:650,
												hideThumbs:200,

												thumbWidth:100,
												thumbHeight:50,
												thumbAmount:2,


												simplifyAll:"off",

												navigationType:"bullet",
												navigationArrows:"solo",
												navigationStyle:"custom",

												touchenabled:"on",
												onHoverStop:"on",
												nextSlideOnWindowFocus:"off",

												swipe_threshold: 0.7,
												swipe_min_touches: 1,
												drag_block_vertical: false,



												keyboardNavigation:"off",

												navigationHAlign:"center",
												navigationVAlign:"bottom",
												navigationHOffset:0,
												navigationVOffset:20,

												soloArrowLeftHalign:"left",
												soloArrowLeftValign:"center",
												soloArrowLeftHOffset:20,
												soloArrowLeftVOffset:0,

												soloArrowRightHalign:"right",
												soloArrowRightValign:"center",
												soloArrowRightHOffset:20,
												soloArrowRightVOffset:0,

												shadow:0,
												fullWidth:"on",
												fullScreen:"off",

												spinner:"spinner0",

												stopLoop:"off",
												stopAfterLoops:-1,
												stopAtSlide:-1,

												shuffle:"off",

												autoHeight:"off",
												forceFullWidth:"off",


												hideTimerBar:"on",
												hideThumbsOnMobile:"off",
												hideNavDelayOnMobile:1500,
												hideBulletsOnMobile:"off",
												hideArrowsOnMobile:"off",
												hideThumbsUnderResolution:0,

												hideSliderAtLimit:0,
												hideCaptionAtLimit:0,
												hideAllCaptionAtLilmit:0,
												startWithSlide:0					});
							});	/*ready*/
							</script>
						</div>
				</div> <!-- END REV SLIDER -->
			</header>
			
			<div id="page-content">
				<div class="fullpage">
					<div id="content-wrap" class="fullwidth">
						<div class="row nospace">
							<div id="content" class="col-md-12">
								<div class="content-inner entry-content">
									<div class="section cover">
										<div class="container">
											<div class="row nornal">
												<div class="vc_span12">
													<div class="element ">
														<div class="heading-area bottom-30 text-center">
															<h4 class="heading large">About <strong>Mahko International</strong></h4>
															<p><span class="sub-heading">global sourcing centre where clients place trusts in us to source for the best deal</span>
														</div>
														<div class="row clearfix normal">
															<div class="vc_span4 wpb_column column_container bottom-xs-30 bottom-sm-30">
																<div class="element ">
																	<div class="iconbox clearfix alt ">
																		<div class="iconbox-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/mahko-icon.png" alt=""></div>
																		<div class="iconbox-content">
																			<h4 class="bottom-10">About Us</h4>
																			<div class="iconbox-content-p"><strong>MAHKO INTERNATIONAL</strong> was first registered in 2003 to provide services and products in Singapore and Malaysia market only. Over the years, the customer base had since expanded across the globe and clients now stretched from Asia, Africa, Europe and South America. In 2011, MAHKO INTERNATIONAL was privatised to provide a professional and global presence in the world market.</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="vc_span4 wpb_column column_container bottom-xs-30 bottom-sm-30">
																<div class="element ">
																	<div class="iconbox clearfix alt ">
																		<div class="iconbox-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/business-icon.png" alt=""></div>
																		<div class="iconbox-content"><h4 class="bottom-10">Our Portfolio</h4>
																			<div class="iconbox-content-p"><strong>MAHKO INTERNATIONAL PTE LTD</strong> is a global sourcing centre where clients place trusts in us to source for the best deal, not only in terms of Pricing, but also Quality Assurance. Over the years, the company has established very close partnership with many factories, in Middle East and Asia, supplying to clients worldwide.</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="vc_span4 wpb_column column_container bottom-xs-30 bottom-sm-30">
																<div class="element ">
																	<div class="iconbox clearfix alt ">
																		<div class="iconbox-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/location-icon.png" alt=""></div>
																		<div class="iconbox-content"><h4 class="bottom-10">Where We Are</h4>
																			<div class="iconbox-content-p"><strong>MAHKO INTERNATIONAL PTE LTD</strong> Head Office is located in SINGAPORE. Being the main Financial and Travel hub in Asia, Singapore is easily accessible to suppliers and customers across the world. Becoming the bridge connecting business between  East and West.</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> 
							</div> 
						</div> 
					</div> 
				</div> 
			</div> 
			<!-- END PAGE CONTENT -->

<?php get_footer(); ?>
