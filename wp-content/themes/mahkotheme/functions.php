<?php
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }

    //enable Post Thumbnails for Post, Page
    add_theme_support( 'post-thumbnails', array( 'page', 'post') );          // Pages only

    //define a new navigation menu
    if(function_exists('register_nav_menus')){
        register_nav_menus (array(
            'main_nav' => 'Main Navigation Menu'
        ));
    }

    //get the current page'parent page id 
    function get_top_parent_page_id() { 
        global $post; 
     
        if ($post->ancestors) { 
            return end($post->ancestors); 
        } else { 
            return $post->ID; 
        } 
    }

    
    /**
     * Required: set 'ot_theme_mode' filter to true.
     */
    add_filter( 'ot_theme_mode', '__return_true' );

    /**
     * Required: include OptionTree.
     */
    require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );

    /**
     * Theme Options
     */
    require( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );

?>