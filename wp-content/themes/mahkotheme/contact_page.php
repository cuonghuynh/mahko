<?php
/*
Template Name: Contact Page
*/
?>

<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				<div class="container">
					<?php 
						$thumb_url = '';
						if (has_post_thumbnail()) {
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
							$thumb_url = $thumb_url[0];
						} else {
							$thumb_url = get_template_directory_uri() . 'images/Flat-&-Processed-Glass/Flat-&-Processed-Glass-bg.jpg';
						}

					?>
					<div class="wp-page-header" style="background-image: url('<?php echo $thumb_url; ?>')">
						<div class="wp-page-title">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="wp-page-nav">
							<ul>
								<li>
									<a href="/">Home</a>
								</li>
								<li>
									<a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
								</li>
							</ul>
						</div>
					</div>
				</div> <!-- END REV SLIDER -->
			</header>
			<div id="page-content">
				<div class="container">
					<div class="contact-holder">
						<div class="row">
							<div class="col-md-6">
								<div class="frame">
									<?php the_content(); ?>
									<div class="social-group">
										<ul>
											<?php 

												$twitter 	= get_field('twitter');
												$instagram 	= get_field('instagram');
												$dribbble 	= get_field('dribbble');
												$facebook 	= get_field('facebook');
												$youtube 	= get_field('youtube');
												$gplus 		= get_field('gplus');
												$vine 		= get_field('vine');

												if (!empty($twitter)) {
													echo '<li><a href="' . $twitter . '"><i class="fa fa-twitter"></i></a></li>';
												}

												if (!empty($instagram)) {
													echo '<li><a href="' . $instagram . '"><i class="fa fa-instagram"></i></a></li>';
												}

												if (!empty($dribbble)) {
													echo '<li><a href="' . $dribbble . '"><i class="fa fa-dribbble"></i></a></li>';
												}

												if (!empty($facebook)) {
													echo '<li><a href="' . $facebook . '"><i class="fa fa-facebook"></i></a></li>';
												}

												if (!empty($youtube)) {
													echo '<li><a href="' . $youtube . '"><i class="fa fa-youtube"></i></a></li>';
												}

												if (!empty($gplus)) {
													echo '<li><a href="' . $gplus . '"><i class="fa fa-google-plus"></i></a></li>';
												}

												if (!empty($vine)) {
													echo '<li><a href="' . $vine . '"><i class="fa fa-vine"></i></a></li>';
												}
											?>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="frame">
									<?php 

									$location = get_field('location');

									if( !empty($location) ):
									?>
										<?php 

										$location = get_field('location');

										if( !empty($location) ):
										?>
										<div class="acf-map" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-address="<?php echo $location['address']; ?>">
											
										</div>
										<?php endif; ?>
										<style type="text/css">

										.acf-map {
											width: 100%;
											height: 400px;
											border: #ccc solid 1px;
										}

										</style>
										
										<script type="text/javascript">
											(function($) {

											function render_map( el ) {
												var lat = el.data('lat');
												var lng = el.data('lng');
												var add = el.data('address');
												var data = [{
															        lat: lat,
															        lon: lng,
															        zoom: 16,
															        title: add,
															        icon: 'http://www.google.com/mapfiles/markerA.png'
															    }
															];
												new Maplace({
												    locations: data,
												    map_div: '.acf-map',
												    controls_type: 'list',
												    controls_on_map: false
												}).Load();
											}

											$(document).ready(function(){

												render_map($(this).find('.acf-map'));
											});

											})(jQuery);
										</script>

									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<style type="text/css">
					.acf-map {
						width: 100%;
						height: 400px;
						border: #ccc solid 1px;
					}
				</style>
				<?php

				// check if the repeater field has rows of data
				if( have_rows('acf_branches') ):
				
					echo '<div class="address-list">';
						echo '<div class="container">';
							echo '<div class="row">';
				
				 	// loop through the rows of data
				    while ( have_rows('acf_branches') ) : the_row();
				        // display a sub field value
				        $name = get_sub_field('name');
				        $address = get_sub_field('address');
				
				        echo '<div class="col-md-4 address-box">';
							echo '<h4>' . $name . '</h4>';
							echo '<p>' . $address . '</p>';
						echo '</div>';
				
				    endwhile;
				
							echo '</div>';
						echo '</div>';
					echo '</div> ';
				
				else :

				    // no rows found

				endif;

				?>
		
			</div> 
			<!-- END PAGE CONTENT -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>

