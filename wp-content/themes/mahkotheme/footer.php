			</div>
			<!-- END INNER CONTENT -->
			<footer id="footer">
				
				<div class="footer-credits<?php if (!is_front_page()) { echo " page"; } ?>">
					<div class="container">
						<div class="footer-credits-inner">
							<div class="row">
								<div class="col-md-6">
									<span>&copy; Copyright <span style="text-transform: uppercase;"><?php bloginfo('name'); ?></span> 2014. </span>
								</div> 
							</div> 
						</div> 
					</div> 
				</div> 
			</footer>
			<!-- END FOOTER -->
			<div class="sb-slidebar sb-right">
				<div class="sb-menu-trigger"></div>
			</div>
			<!-- END SIDEBAR RIGHT -->
		</div> 
		<div class="revsliderstyles">
			<style type="text/css">
				.tp-caption.erika_big_title{color:#EF4A43;text-shadow:none;background-color:transparent;text-decoration:none;font-size:72px;line-height:1;font-weight:800;text-transform:uppercase;border-width:0px;border-color:rgb(0,0,0);border-style:none}
				.tp-caption.erika_small_title{color:#272727;text-shadow:none;background-color:transparent;text-decoration:none;text-transform:uppercase;letter-spacing:0.2em;font-weight:600;font-size:14px;border-width:0px;border-color:rgb(39,39,39);border-style:none}
				.tp-caption.erika_p{color:#999;font-size:14px;line-height:23px;text-shadow:none;background-color:transparent;text-decoration:none;text-align:center;border-width:0px;border-color:rgb(0,0,0);border-style:none}
				.tp-caption.erika_p1{color:#999;font-size:14px;line-height:23px;text-shadow:none;background-color:transparent;text-decoration:none;text-align:right;border-width:0px;border-color:rgb(0,0,0);border-style:none}
				.erika_big_title{color:#EF4A43;text-shadow:none;background-color:transparent;text-decoration:none;font-size:72px;line-height:1;font-weight:800;text-transform:uppercase;border-width:0px;border-color:rgb(0,0,0);border-style:none}
				.erika_small_title{color:#272727;text-shadow:none;background-color:transparent;text-decoration:none;text-transform:uppercase;letter-spacing:0.2em;font-weight:600;font-size:14px;border-width:0px;border-color:rgb(39,39,39);border-style:none}
				.erika_p{color:#999;font-size:14px;line-height:23px;text-shadow:none;background-color:transparent;text-decoration:none;text-align:center;border-width:0px;border-color:rgb(0,0,0);border-style:none}
				.erika_p1{color:#999;font-size:14px;line-height:23px;text-shadow:none;background-color:transparent;text-decoration:none;text-align:right;border-width:0px;border-color:rgb(0,0,0);border-style:none}
			</style>
		</div>
		
		<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/minify-b1-tp-tools-e59bc13a67fe21c4e9be8500ac69cd12.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/minify-b1-jquery-form-cfbc811880f8185c626c9da0ad56ce90.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/minify-b1-jquery.infinite-ffabef03779976edc8c89918b679cb89.js'></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/maplace-0.1.3.min.js'></script>

	<?php wp_footer(); ?>
	
	<!-- Don't forget analytics -->
	
</body>

</html>
