<?php
/*
Template Name: Product Page
*/
?>

<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				<div class="container">
					<?php

						$parent_id = get_top_parent_page_id();

						$current_id = get_the_id();

						$title = '';
						$pamerlink = '';
						$thumb_url = '';

						if ($parent_id != $current_id) {

							$thumb_id = get_post_thumbnail_id($parent_id);
							$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
							$thumb_url = $thumb_url[0];

							$title = get_the_title($parent_id);

							$pamerlink = get_permalink($parent_id);
						} else {

							$title = get_the_title();
							$pamerlink = get_permalink();

							if (has_post_thumbnail()) {
								$thumb_id = get_post_thumbnail_id();
								$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
								$thumb_url = $thumb_url[0];
							} else {
								$thumb_url = get_template_directory_uri() . 'images/Flat-&-Processed-Glass/Flat-&-Processed-Glass-bg.jpg';
							}

						}
					?>
					<div class="wp-page-header" style="background-image: url('<?php echo $thumb_url; ?>')">
						<div class="wp-page-title">
							<h1><?php echo $title; ?></h1>
						</div>
						<div class="wp-page-nav">
							<ul>
								<li>
									<a href="<?php echo get_site_url(); ?>">Home</a>
								</li>
								<li>
									<a href="<?php echo $pamerlink; ?>"><?php echo $title; ?></a>
								</li>
							</ul>
						</div>
					</div>
				</div> <!-- END REV SLIDER -->
			</header>
			<div id="page-content">
				<div class="container">
					<?php the_content(); ?>
					<?php 
						$id = get_the_id();
						$args = array (
								'child_of' 		=> $id,
								'post_type'		=> 'page',
								'echo'			=> 1,
							);
						echo '<ul>';
						wp_list_pages( $args );
						echo '</ul>';
					?>
				</div>			
			</div> 
			<!-- END PAGE CONTENT -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>

