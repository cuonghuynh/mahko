
<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				<div class="container">
					<?php 
						$thumb_url = '';
						if (has_post_thumbnail()) {
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
							$thumb_url = $thumb_url[0];
						} else {
							$thumb_url = get_template_directory_uri() . 'images/Flat-&-Processed-Glass/Flat-&-Processed-Glass-bg.jpg';
						}

					?>
					<div class="wp-page-header" style="background-image: url('<?php echo $thumb_url; ?>')">
						<div class="wp-page-title">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="wp-page-nav">
							<ul>
								<li>
									<a href="/">Home</a>
								</li>
								<li>
									<a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
								</li>
							</ul>
						</div>
					</div>
				</div> <!-- END REV SLIDER -->
			</header>
			<div id="page-content">
				<div class="container">
					<?php the_content(); ?>
				</div>			
			</div> 
			<!-- END PAGE CONTENT -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>

