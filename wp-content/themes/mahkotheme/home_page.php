<?php
/*
Template Name: Home Page
*/
?>

<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				<div class="container" id="home-page">
					<?php the_content(); ?>
				</div> <!-- END REV SLIDER -->
			</header>
			
			<div id="page-content home">
				<div class="fullpage">
					<div id="content-wrap" class="fullwidth">
						<div class="row nospace">
							<div id="content" class="col-md-12">
								<div class="content-inner entry-content">
									<div class="section cover">
										<div class="container">
											<div class="row nornal">
												<div class="vc_span12">
													<div class="element ">
														<div class="heading-area bottom-30 text-center">
															<?php 

																$header 	= get_field('header');
																$sub_header = get_field('sub_header') ;
															?>
															<h4 class="heading large"><?php echo $header; ?></h4>
															<p><span class="sub-heading"><?php echo $sub_header; ?></span>
														</div>
														<div class="row clearfix normal">
															<?php

															// check if the repeater field has rows of data
															if( have_rows('about_mahko') ):
																
															 	// loop through the rows of data
															    while ( have_rows('about_mahko') ) : the_row();
																	$img = get_sub_field('thumb_image');
																	$title = get_sub_field('title');
																	$content = get_sub_field('content');

																	echo '<div class="vc_span4 wpb_column column_container bottom-xs-30 bottom-sm-30">';
																		echo '<div class="element ">';
																			echo '<div class="iconbox clearfix alt ">';
																				echo '<div class="iconbox-icon"><img src="' . $img . '" alt=""></div>';
																				echo '<div class="iconbox-content">';
																					echo '<h4 class="bottom-10">' . $title . '</h4>';
																					echo '<div class="iconbox-content-p">' . $content . '</div>';
																				echo '</div>';
																			echo '</div>';
																		echo '</div>';
																	echo '</div>';
															    endwhile;
															
															else :

															    // no rows found

															endif;

															?>
															
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> 
							</div> 
						</div> 
					</div> 
				</div> 
			</div> 
			<!-- END PAGE CONTENT -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
